<?php


namespace App;


use Symfony\Component\HttpFoundation\Response;

abstract class Controller
{
    protected $viewDir = '';
    protected $layoutDir = __DIR__.'/Views/layouts/';
    protected $layout = 'main.php';
    public function __construct($mvc)
    {
        $className = explode('Controller',end(explode('\\', get_called_class())));
        if($controllerName =$className){
            $this->viewDir = __DIR__.'/Views/'.strtolower($controllerName[0]);
        }
    }

    /**
     * @param $view string
     * @param $data array
     */
    protected function render($view, $data)
    {
        $view_file = $this->viewDir."/{$view}";
        ob_start();
        extract($data);
        include ($view_file);
        $content = ob_get_contents();
        ob_clean();
        $content = $this->renderLayout($content);
        $response = new Response($content);
        $response->send();
    }

    private function renderLayout($content){
        ob_start();
        $layoutPath = $this->layoutDir.$this->layout;
        include ($layoutPath);
        $content = ob_get_contents();
        ob_clean();
        return $content;
    }
}