<?php


namespace App\Controllers;


use App\Controller;
use App\Models\User;
use App\Mvc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class AdminController extends Controller
{
    /**
     * @param $request Request
     */
    public function login($request)
    {
        $errors = [];
        $user = new User();
        if ($request->isMethod('POST')) {
            $user->load($request->request->all());
            if ($user->validate()) {
                if ($user->login()) {
                    header('Location: /');
                }
            }
        }
        $errors = $user->getErrors();
        $this->render('login.php', [
            'errors' => $errors,
            'user' => $user
        ]);
    }

    /**
     * @param $requet
     * @param $session Session
     */
    public function logout()
    {
        Mvc::logout();
    }
}