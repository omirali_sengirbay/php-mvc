<?php
namespace App\Controllers;

use App\Controller;
use App\Models\Task;
use App\Mvc;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public $perPageSize = 3;

    /**
     * @param $request Request
     * @throws \Exception
     */
    public function index($request){
        $offset = ($request->get('page') && $request->get('page') > 1) ? (($request->get('page') - 1) * $this->perPageSize) : 0;
        $count_all = count(Task::sql("SELECT * FROM :database.:table"));
        $tasks = Task::sql("SELECT * FROM :database.:table ORDER BY username LIMIT $offset,{$this->perPageSize}");
        $message = '';
        if (isset($_SESSION['message'])) {
            $message = $_SESSION['message'];
            unset($_SESSION['message']);
        }
        $this->render('index.php',[
            'tasks' => $tasks,
            'count_all' => $count_all,
            'offset' => $offset,
            'perPageSize' => $this->perPageSize,
            'page' => $request->get('page'),
            'message' => $message,
        ]);
    }

    /**
     * @param $request Request
     */
    public function add($request){
        $task = new Task();
        $errors = [];
        if($request->isMethod('POST')){
            $task->load($request->request->all());
            if($task->validate()){
                $task->done = 0;
                $task->change_by_admin = 0;
                $task->save();
                $_SESSION['message'] = "Task successfully added!";
                header('Location: /');
            }else{
                $errors = $task->getErrors();
            }
        }
        $this->render('add.php',['task' => $task, 'errors' => $errors]);
    }
    /**
     * @param $request Request
     */
    public function update($request){
        if(is_null(Mvc::identity())){
            header('Location: /admin/login');
            exit;
        }
        $id = $request->get('id');
        $post = $request->request->all();
        $task = Task::retrieveByPK($id);
        if(Mvc::identity()->id && $post['text'] != $task->text){
            $task->change_by_admin = 1;
        }
        $errors = [];
        if($request->isMethod('POST')){
            if(is_null(Mvc::identity())){
                print_r(Mvc::identity());die;
                header('Location: /admin/login');
            }
            $task->load($post);
            if($task->validate()){
                $task->save();
                $_SESSION['message'] = "Task successfully updated!";
                header('Location: /');
            }else{
                $errors = $task->getErrors();
            }
        }
        $this->render('update.php',['task' => $task, 'errors' => $errors]);
    }

    /**
     * @param $request Request
     */
    public function done($request){
        if(!Mvc::identity()->id){
            header('Location: /');
        }
        $id = $request->get('id');
        $task = Task::retrieveByPK($id);
        $task->done = 1;
        $task->save();
        header('Location: /');
    }
}