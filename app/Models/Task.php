<?php

namespace App\Models;

use mysqli;

/**
 * Class Task
 * @package App\Models
 */
class Task extends \SimpleOrm
{
    /**
     * @var int Not performed status
     */
    const STATUS_NOT_PERFORMED = 0;

    /**
     * @var int Done status
     */
    const STATUS_DONE = 1;

    /**
     * @var int
     */
    const CHANGE_BY_ADMIN = 1;

    /**
     * @var string ORM setup's
     */
    protected static
        $database = 'omirali',
        $table = 'tasks',
        $pk = 'id';

    /**
     * @var int
     */
    public $id;

    /**
     * @var string Username
     */
    public $username;

    /**
     * @var string User email
     */
    public $email;

    /**
     * @var string Task text
     */
    public $text;

    /**
     * @var int Task status
     */
    public $done;

    /**
     * @var Task changed by admin
     */
    public $change_by_admin;

    /**
     * @return array Model attribute rules
     */
    public function rules()
    {
        return [
            'username' => ['string', 'required', 32],
            'email' => ['email', 'required', 32],
            'text' => ['string', 'required']
        ];
    }

    public static function statusLabels($status)
    {
        $statuses = [
            self::STATUS_NOT_PERFORMED => 'Not performed',
            self::STATUS_DONE => 'Done',
        ];
        if (array_key_exists($status, $statuses)) {
            return $statuses[$status];
        }
        return $statuses;
    }
}