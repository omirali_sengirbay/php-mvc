<?php


namespace App\Models;


use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class User
 * @package App\Models
 * @var $id int
 * @var $username string
 * @var $password string
 * @var $auth_key string
 */
class User extends \SimpleOrm
{
    protected static
        $database = 'omirali',
        $table = 'users',
        $pk = 'id';
    public $id;
    public $username;
    public $password;
    public $auth_key;

    /**
     * @return array User model rules
     */
    public function rules(){
        return [
            'username' => ['string','required',32],
            'password' => ['string','required',255]
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function login(){
        $password = md5($this->password);

        $sql = "SELECT * FROM :table WHERE username = '{$this->username}' AND password = '{$password}'";
        if($user = self::sql($sql, \SimpleOrm::FETCH_ONE)){
            $auth_key = md5($password+time());
            $user->set('auth_key',$auth_key);
            $user->save();
            $_SESSION['identity'] = $auth_key;
            return true;

        }
        $this->setError('username', 'Invalid username or password!');
        return false;
    }
}