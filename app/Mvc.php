<?php


namespace App;

use App\Models\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Mvc
 * @package App
 */
class Mvc
{

    public $config;
    protected $routes = [];
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->routes = $config['request']['routes'];
        // Connect to the database using mysqli
        $conn = new \mysqli($this->config['db']['server'] , $this->config['db']['username'], $this->config['db']['password'], $this->config['db']['dbname']);

        if ($conn->connect_error)
            die(sprintf('Unable to connect to the database. %s', $conn->connect_error));

        \SimpleOrm::useConnection($conn, 'database');
    }

    public static function identity(){
        if(isset($_SESSION['identity']) && $auth_user = User::retrieveByField('auth_key', $_SESSION['identity'], \SimpleOrm::FETCH_ONE)){
            return $auth_user;
        }
        return null;
    }

    public static function logout(){
        session_start();
        unset($_SESSION['identity']);
        session_destroy();
        header('Location: /admin/login');
    }

    public function run(){

        $request = Request::createFromGlobals();
        if(array_key_exists($request->getPathInfo(),$this->routes)){
            $handler = $this->routes[$request->getPathInfo()];
            $controller = new $handler['controller']($this);
            call_user_func([$controller, $handler['action']], $request);
        }
    }
}