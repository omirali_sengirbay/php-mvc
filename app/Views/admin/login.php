<div class="row">
    <div class="col-lg-6">
        <h1 class="mt-5">Sign In</h1>
<?php if(count($errors) > 0):?>
    <div class="alert alert-danger">
        <?php foreach ($errors as $error):?>
            <p><?=$error?></p>
        <?php endforeach;?>
    </div>
<?php endif;?>
<form method="post">
    <div class="form-group">
        <label for="inputUsername">Username</label>
        <input name="username" value="<?=htmlspecialchars($user->username)?>" type="text" class="form-control" id="inputUsername" placeholder="Username">
    </div>
    <div class="form-group">
        <label for="inputPassword">Password</label>
        <input name="password" type="password" class="form-control" id="inputPassword" aria-describedby="emailHelp" placeholder="Enter password">
    </div>
    <button type="submit" class="btn btn-primary">Login</button>
</form>
</div>
</div>