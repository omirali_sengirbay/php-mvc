<div class="row">
    <div class="col-lg-12">
        <h1 class="mt-5">Task list <a href="/add" class="btn btn-primary">Add new</a></h1>
        <?php if ($message): ?>
            <div class="alert alert-success">
                <p><?= $message ?></p>
            </div>
        <?php endif; ?>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Username</th>
                <th scope="col">E-mail</th>
                <th scope="col">Text</th>
                <th scope="col"></th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tasks as $task): ?>
                <tr>
                    <th scope="row"><?= $task->id ?></th>
                    <td><?= htmlspecialchars($task->username) ?></td>
                    <td><?= htmlspecialchars($task->email) ?></td>
                    <td>
                        <?= htmlspecialchars($task->text) ?>
                        <?php if ($task->change_by_admin == 1): ?>
                            <br><span class="badge badge-primary">Changed by admin</span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($task->done == 1): ?>
                            <p class="text-success"><?= \App\Models\Task::statusLabels($task->done); ?></p>
                        <?php else: ?>
                            <p class="text-dark"><?= \App\Models\Task::statusLabels($task->done); ?></p>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (\App\Mvc::identity()->id && $task->done != 1): ?>
                            <a class="btn btn-link" href="/update?id=<?= $task->id ?>">Update</a>
                        <?php endif; ?>
                        <?php if (\App\Mvc::identity()->id && $task->done != 1): ?>
                            <a class="btn btn-link" href="/done?id=<?= $task->id ?>">Done</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <thead>
            <tr>
                <td colspan="5">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <?php for ($i = 0; $i < ceil($count_all / $perPageSize); $i++): ?>
                                <li class="page-item <?= ($page == ($i + 1)) ? 'active' : '' ?>"><a class="page-link"
                                                                                                    href="/?page=<?= ($i + 1) ?>"><?= ($i + 1) ?></a>
                                </li>
                            <?php endfor; ?>
                            <!--                              <li class="page-item active">-->
                            <!--                                  <a class="page-link" href="/?page=1">2 <span class="sr-only">(current)</span></a>-->
                            <!--                              </li>-->
                            <!--                              <li class="page-item"><a class="page-link" href="/?page=1">3</a></li>-->
                        </ul>
                    </nav>
                </td>
            </tr>
            </thead>
        </table>
    </div>
</div>
