<div class="row">
    <div class="col-lg-12">
        <h1 class="mt-5">Update task #<?=$task->id?></h1>
        <?php if(count($errors) > 0):?>
            <div class="alert alert-danger">
            <?php foreach ($errors as $error):?>
                <p><?=$error?></p>
            <?php endforeach;?>
            </div>
        <?php endif;?>
        <form method="post">
            <div class="form-group">
                <label for="inputUsername">Username</label>
                <input name="username" value="<?=htmlspecialchars($task->username)?>" type="text" class="form-control" id="inputUsername" placeholder="Username">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input name="email" value="<?=htmlspecialchars($task->email)?>" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="inputText">Text</label>
                <textarea name="text" class="form-control" id="inputText" placeholder="Text"><?=htmlspecialchars($task->text)?></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>