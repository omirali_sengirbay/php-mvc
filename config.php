<?php
return [
    'db' => [
        'dbname' => 'db',
        'server' => 'localhost',
        'username' => 'root',
        'password' => 'root'
    ],
    'request' => [
        'routes' => [
            '/'=> [
                'controller' => 'App\Controllers\DefaultController',
                'action'=>'index'
            ],
            '/add'=> [
                'controller' => 'App\Controllers\DefaultController',
                'action'=>'add'
            ],
            '/update'=> [
                'controller' => 'App\Controllers\DefaultController',
                'action'=>'update'
            ],
            '/done'=> [
                'controller' => 'App\Controllers\DefaultController',
                'action'=>'done'
            ],
            '/admin/login'=> [
                'controller' => 'App\Controllers\AdminController',
                'action'=>'login'
            ],
            '/admin/logout'=> [
                'controller' => 'App\Controllers\AdminController',
                'action'=>'logout'
            ],
        ]
    ]
];