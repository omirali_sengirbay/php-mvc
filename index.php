<?php
if(!isset($_SESSION['identity'])){
    session_start();
}
include 'vendor/autoload.php';
include 'libs/SimpleOrm.php';
use App\Mvc;
use Symfony\Component\HttpFoundation\Session\Session;

$config = require_once 'config.php';
(new Mvc($config))->run();